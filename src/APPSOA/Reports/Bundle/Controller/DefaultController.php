<?php

namespace APPSOA\Reports\Bundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Route("/reports", name="reports")
     * @Template()
     */
    public function indexAction($name)
    {
        return array('name' => $name);
    }
}
