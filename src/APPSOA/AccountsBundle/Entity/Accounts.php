<?php

namespace APPSOA\AccountsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Accounts
 *
 * @ORM\Table(name="accounts")
 * @ORM\Entity
 */
class Accounts
{

    /**
     * @var string
     *
     */
    private $recordtype = 'account';
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="parentId", type="integer", nullable=true)
     */
    private $parentid;
    /**
     * @var string
     *
     * @ORM\Column(name="accountName", type="string", length=255, nullable=true)
     *
     * @Assert\NotBlank()
     * @Assert\Length(min=2)
     *
     */
    private $accountname;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=false, columnDefinition="ENUM('ACTIVE','PENDING','DISABLED','LOCKED')")
     *
     * @Assert\NotBlank()
     * @Assert\Choice(choices = {"ACTIVE", "PENDING", "DISABLED", "LOCKED"}, message = "Choose a valid status.")
     *
     */
    private $status;

    public static $statuses = ['ACTIVE','PENDING','DISABLED','LOCKED'];

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function setRecordtype($recordtype) {

        $this->recordtype = $recordtype;

        return $this;
    }
    public function getRecordtype() {

        return $this->recordtype;

    }

    /**
     * Set accountname
     *
     * @param string $accountname
     * @return Accounts
     */
    public function setAccountname($accountname)
    {
        $this->accountname = $accountname;

        return $this;
    }

    /**
     * Get accountname
     *
     * @return string
     */
    public function getAccountname()
    {
        return $this->accountname;
    }

    /**
     * Set parentid
     *
     * @param integer $parentid
     * @return Accounts
     */
    public function setParentid($parentid)
    {

        $this->parentid = $parentid->id;

        return $this;
    }

    /**
     * Get parentid
     *
     * @return integer
     */
    public function getParentid()
    {
        return $this->parentid;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Accounts
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }
}
