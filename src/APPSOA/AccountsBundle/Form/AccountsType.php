<?php

namespace APPSOA\AccountsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AccountsType extends AbstractType
{


    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('parentId', 'entity', [
                'label' => 'Parent Account',
                'required' => false,
                'class' => 'APPSOAAccountsBundle:Accounts',
                'placeholder' => 'Select a parent account/owner..',
                'property' => 'accountname',
                'attr' => ['class' => 'ui dropdown']

            ])
            ->add('recordtype', 'hidden')
            ->add('accountname', 'text', ['label' => 'Account Name', 'required' => true])
            ->add('status', 'choice', ['label' => 'Account Status', 'choices' => ['ACTIVE' => 'ACTIVE', 'PENDING' => 'PENDING', 'DISABLED' => 'DISABLED', 'LOCKED' => 'LOCKED'],
                'attr' => ['class' => 'ui dropdown']
            ]);

    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'APPSOA\AccountsBundle\Entity\Accounts',
            'recordType' => 'Account'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appsoa_accountsbundle_accounts';
    }
}
