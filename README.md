# **DATA CONTROL CENTER** #
## Web Application Demo ##

This project provides CRUD with configurable based objects.
Eliminating the object mapping, SQL configuration and validation headaches the point & click "self-serve" Admin GUI
will get your user input captured and ready for automated workflow.

### Getting the code and configuring: ###

1. Clone/checkout the source repo: ```git clone git@bitbucket.org:itainteasy/appsoa-base.git test1```
2. Change into the new directory: ```cd test1/```
3. Use composer to download our dependencies: ```composer install``` (may take a minute or two)
4. **Creating the "app/config/parameters.yml" file** will come up next and you can enter your database credentials.

### Check to make sure everything is working: ###

1. Run ```php app/check.php``` . you should see "[OK] Your system is ready to run Symfony2 projects"

### Use the built-in webserver and give it a whirl! ###
```
m@turbo /gibson/projects/test1
$ php app/console server:run
Server running on http://127.0.0.1:8000

Quit the server with CONTROL-C.
```
**Now point your browser to http://127.0.0.1:8000/ or http://127.0.0.1:8000/accounts/new**
![create.PNG](https://bitbucket.org/repo/nBA4g8/images/280580906-create.PNG)